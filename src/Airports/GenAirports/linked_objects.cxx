#include <simgear/debug/logstream.hxx>

#include "debug.hxx"
#include "linked_objects.hxx"


Windsock::Windsock(char* definition)
{
    std::istringstream ss(definition);
    ss >> lat >> lon >> lit;

    if (lat < -90 || lat > 90 || lon < -180 || lon > 180) {
	valid = false;
	TG_LOG(SG_GENERAL, SG_ALERT, "Invalid windsock line: " << definition);
    }

    TG_LOG(SG_GENERAL, SG_DEBUG, "Read Windsock: (" << lon << "," << lat << ") lit: " << lit);
}

Beacon::Beacon(char* definition)
{
    std::istringstream ss(definition);
    ss >> lat >> lon >> code;

    if (lat < -90 || lat > 90 || lon < -180 || lon > 180) {
	valid = false;
	TG_LOG(SG_GENERAL, SG_ALERT, "Invalid beacon line: " << definition);
    }

    TG_LOG(SG_GENERAL, SG_DEBUG, "Read Beacon: (" << lon << "," << lat << ") code: " << code);
}

Sign::Sign(char* definition)
{
    std::string sgdef;
    double def_heading;

    std::istringstream ss(definition);
    ss >> lat >> lon >> def_heading >> reserved >> size >> sgdef;

    if (lat < -90 || lat > 90 || lon < -180 || lon > 180 || size < 1 || size > 5) {
	valid = false;
	TG_LOG(SG_GENERAL, SG_ALERT, "Invalid sign line: " << definition);
    }

    // 850 format sign heading is the heading which points away from the visible numbers
    // Flightgear wants the heading to be the heading in which the sign is read
    heading = -def_heading + 360.0;

    TG_LOG(SG_GENERAL, SG_DEBUG, "Read Sign: (" << lon << "," << lat << ") heading " << def_heading << " size " << size << " definition: " << sgdef << " calc view heading: " << heading);

    sgn_def = sgdef;
}
